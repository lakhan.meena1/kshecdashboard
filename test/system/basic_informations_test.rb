require "application_system_test_case"

class BasicInformationsTest < ApplicationSystemTestCase
  setup do
    @basic_information = basic_informations(:one)
  end

  test "visiting the index" do
    visit basic_informations_url
    assert_selector "h1", text: "Basic information"
  end

  test "should create basic information" do
    visit basic_informations_url
    click_on "New basic information"

    click_on "Create Basic information"

    assert_text "Basic information was successfully created"
    click_on "Back"
  end

  test "should update Basic information" do
    visit basic_information_url(@basic_information)
    click_on "Edit this basic information", match: :first

    click_on "Update Basic information"

    assert_text "Basic information was successfully updated"
    click_on "Back"
  end

  test "should destroy Basic information" do
    visit basic_information_url(@basic_information)
    click_on "Destroy this basic information", match: :first

    assert_text "Basic information was successfully destroyed"
  end
end
