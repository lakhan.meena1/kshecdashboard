Rails.application.routes.draw do
  root "dashboard#index"
  resources :basic_informations
  resources :infrastructures
  
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    passwords: 'users/passwords',
    registrations: 'users/registrations'
  }
end
