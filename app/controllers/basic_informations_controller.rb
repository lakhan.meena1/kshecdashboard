class BasicInformationsController < ApplicationController
  before_action :set_basic_information, only: %i[ show edit update destroy ]

  def index
    @basic_informations = BasicInformation.all.order(created_at: :desc)
  end

  def show
  end

  def new
    @basic_information = BasicInformation.new
  end

  def edit
  end

  def create
    @basic_information = BasicInformation.new(basic_information_params)
    @basic_information.user_id = current_user.id
    respond_to do |format|
      if @basic_information.save
        format.html { redirect_to basic_informations_url, notice: "Basic information was successfully created." }
        format.json { render :show, status: :created, location: @basic_information }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @basic_information.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @basic_information.update(basic_information_params)
        format.html { redirect_to basic_informations_url, notice: "Basic information was successfully updated." }
        format.json { render :show, status: :ok, location: @basic_information }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @basic_information.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @basic_information.destroy
    respond_to do |format|
      format.html { redirect_to basic_informations_url, notice: "Basic information was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_basic_information
      @basic_information = BasicInformation.find_by(id: params[:id])
    end

    def basic_information_params
      params.require(:basic_information).permit(:university_name, :date_of_establishment, :date_of_operationalize, :grade, :valid_upto, :nirf_rank, :jurisdiction, :taluk, :no_of_affiliated_colleges, :no_of_program_offered_ug, :no_of_program_offered_pg, :no_of_program_offered_phd, :no_of_students_ug, :no_of_students_pg, :no_of_students_phd)
    end
end
