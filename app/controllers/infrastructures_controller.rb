class InfrastructuresController < ApplicationController
  before_action :set_infrastructure, only: %i[ show edit update destroy ]

  def index
    @infrastructures = Infrastructure.all.order(created_at: :desc)
  end

  def show
  end

  def new
    @infrastructure = Infrastructure.new
  end

  def edit
  end

  def create
    @infrastructure = Infrastructure.new(infrastructure_params)
    @infrastructure.user_id = current_user.id
    respond_to do |format|
      if @infrastructure.save
        format.html { redirect_to infrastructures_url, notice: "Infrastructure was successfully created." }
        format.json { render :show, status: :created, location: @infrastructure }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @infrastructure.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @infrastructure.update(infrastructure_params)
        format.html { redirect_to infrastructures_url, notice: "Infrastructure was successfully updated." }
        format.json { render :show, status: :ok, location: @infrastructure }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @infrastructure.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @infrastructure.destroy
    respond_to do |format|
      format.html { redirect_to infrastructures_url, notice: "Infrastructure was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_infrastructure
      @infrastructure = Infrastructure.find_by(id: params[:id])
    end

    # Only allow a list of trusted parameters through.
    def infrastructure_params
      params.require(:infrastructure).permit(:land_availability, :main_campus, :pg_center_1, :pg_center_2, :pg_center_3, :no_of_class_rooms, :no_of_labs)
    end
end
