module ApplicationHelper
	def is_same_route?(url)
  	if request.env['PATH_INFO'].include?(url)
  		return "nav-link px-0 active"
  	else
  		return "nav-link px-0"
  	end
  end
end
