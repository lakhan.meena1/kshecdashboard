class CreateBasicInformations < ActiveRecord::Migration[7.0]
  def change
    create_table :basic_informations do |t|
      t.string :university_name
      t.date :date_of_establishment
      t.date :date_of_operationalize
      t.string :grade
      t.date :valid_upto
      t.string :nirf_rank
      t.string :jurisdiction
      t.string :taluk
      t.integer :no_of_affiliated_colleges, default: 0
      t.integer :no_of_program_offered_ug, default: 0
      t.integer :no_of_program_offered_pg, default: 0
      t.integer :no_of_program_offered_phd, default: 0
      t.integer :no_of_students_ug, default: 0
      t.integer :no_of_students_pg, default: 0
      t.integer :no_of_students_phd, default: 0
      t.references :user
      t.timestamps
    end
  end
end
