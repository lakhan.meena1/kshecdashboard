class CreateInfrastructures < ActiveRecord::Migration[7.0]
  def change
    create_table :infrastructures do |t|
      t.string :land_availability 
      t.string :main_campus 
      t.string :pg_center_1
      t.string :pg_center_2 
      t.string :pg_center_3 
      t.integer :no_of_class_rooms , default: 0
      t.integer :no_of_labs, default: 0
      t.references :user
      t.timestamps
    end
  end
end
