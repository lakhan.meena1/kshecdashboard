# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_01_05_112520) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "basic_informations", force: :cascade do |t|
    t.string "university_name"
    t.date "date_of_establishment"
    t.date "date_of_operationalize"
    t.string "grade"
    t.date "valid_upto"
    t.string "nirf_rank"
    t.string "jurisdiction"
    t.string "taluk"
    t.integer "no_of_affiliated_colleges", default: 0
    t.integer "no_of_program_offered_ug", default: 0
    t.integer "no_of_program_offered_pg", default: 0
    t.integer "no_of_program_offered_phd", default: 0
    t.integer "no_of_students_ug", default: 0
    t.integer "no_of_students_pg", default: 0
    t.integer "no_of_students_phd", default: 0
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_basic_informations_on_user_id"
  end

  create_table "infrastructures", force: :cascade do |t|
    t.string "land_availability"
    t.string "main_campus"
    t.string "pg_center_1"
    t.string "pg_center_2"
    t.string "pg_center_3"
    t.integer "no_of_class_rooms", default: 0
    t.integer "no_of_labs", default: 0
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_infrastructures_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "phone_number"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
